data class Level(val name: String) {
    companion object {
        val ERROR = Level("ERROR")
        val WARNING = Level("WARNING")
        val INFO = Level("INFO")
        val DEBUG = Level("DEBUG")
        val TRACE = Level("TRACE")
    }
}