import java.text.SimpleDateFormat
import java.util.*

data class Logger(
        var level: Level = Level.INFO,
        var loggingCoefficient: Double = 0.5) {

    fun log(msg: String?, level: Level = this.level, date: String? = time) {
        println(String.format("%s [%s] %s", level.name, date, msg))
    }

    fun stochasticLog(msg: String?) {
        if (Math.random() < loggingCoefficient) {
            log(msg)
        }
    }

    companion object {
        val time: String = SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSSSS").format(Calendar.getInstance().time)
    }
}