import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.io.FileNotFoundException
import java.io.IOException
import java.io.PrintStream
import java.nio.file.Files
import java.nio.file.Paths

internal class StochasticLogTest {
    private var logger = Logger()
    private var oldPrintStream = System.out
    private lateinit var newPrintStream: PrintStream

    @BeforeEach
    @Throws(FileNotFoundException::class)
    private fun setOutputStream() {
        newPrintStream = PrintStream(TEXT_FILE_NAME)
        System.setOut(newPrintStream)
    }

    @AfterEach
    private fun returnOutputStream() {
        newPrintStream.close()
        System.setOut(oldPrintStream)
    }

    @AfterEach
    @Throws(IOException::class)
    fun removeTestFiles() {
        Files.delete(Paths.get(TEXT_FILE_NAME))
    }

    @Test
    @Throws(IOException::class)
    fun coefficientTest() {
        logger.loggingCoefficient = Math.random()
        for (i in 0 until COUNT_REPEAT) {
            logger.stochasticLog("some message")
        }
        Assertions.assertTrue(Files.readAllLines(Paths.get(TEXT_FILE_NAME)).size <= COUNT_REPEAT)
    }

    @Test
    @Throws(IOException::class)
    fun minCoefficientTest() {
        logger.loggingCoefficient = 0.0
        for (i in 0 until COUNT_REPEAT) {
            logger.stochasticLog("some message")
        }
        Assertions.assertEquals(0, Files.readAllLines(Paths.get(TEXT_FILE_NAME)).size)
    }

    @Test
    @Throws(IOException::class)
    fun maxCoefficientTest() {
        logger.loggingCoefficient = 1.0
        for (i in 0 until COUNT_REPEAT) {
            logger.stochasticLog("some message")
        }
        Assertions.assertEquals(COUNT_REPEAT, Files.readAllLines(Paths.get(TEXT_FILE_NAME)).size)
    }

    companion object {
        private const val TEXT_FILE_NAME = "output.txt"
        private const val COUNT_REPEAT = 100
    }
}