import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.io.FileNotFoundException
import java.io.IOException
import java.io.PrintStream
import java.nio.file.Files
import java.nio.file.Paths

class LoggerTests {
    private var logger = Logger()
    private var oldPrintStream = System.out
    private lateinit var newPrintStream: PrintStream

    @BeforeEach
    @Throws(FileNotFoundException::class)
    fun setOutputStream() {
        newPrintStream = PrintStream(TEXT_FILE_NAME)
        System.setOut(newPrintStream)
    }

    @AfterEach
    fun returnOutputStream() {
        newPrintStream.close()
        System.setOut(oldPrintStream)
    }

    @AfterEach
    @Throws(IOException::class)
    fun removeTestFiles() {
        Files.delete(Paths.get(TEXT_FILE_NAME))
    }

    @Test
    @Throws(IOException::class)
    fun logTest() {
        logger.log("some text")
        Assertions.assertEquals(1, Files.readAllLines(Paths.get(TEXT_FILE_NAME)).size)
        Assertions.assertTrue(Files.readAllLines(Paths.get(TEXT_FILE_NAME))[0].startsWith("INFO"))
    }

    @Test
    @Throws(IOException::class)
    fun stochasticLogTest() {
        logger.stochasticLog("some text")
        Assertions.assertEquals(1, Files.readAllLines(Paths.get(TEXT_FILE_NAME)).size)
        Assertions.assertTrue(Files.readAllLines(Paths.get(TEXT_FILE_NAME))[0].startsWith("INFO"))
    }

    companion object {
        private const val TEXT_FILE_NAME = "output.txt"
    }
}